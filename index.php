<?php
$config     = 'conf.ini';
$config     = parse_ini_file($config, true);
include("DDoS.php");

$link_mysql = mysqli_connect($config['host'],$config['user'],$config['password'],$config['database']);
if (!$link_mysql) {
    printf("Невозможно подключиться к базе данных. Код ошибки: %s\n", mysqli_connect_error());
    exit;
}
$check= new DDoS($config['time_block'],$config['time_token'],$config['count_request'],$link_mysql);// время блокирования, анализируемый перод, количество запросов в период
$block=$check->is_block($_SERVER['REMOTE_ADDR']);
if ($block) {
    http_response_code(429);
    header('Retry-After: '.$block);

}
else {
    header('Content-Type: text/plain; charset=UTF-8');
    echo "Hello world!";
}
mysqli_close($link_mysql);