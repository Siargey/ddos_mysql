<?php
class DDoS
{
    private $time_block; // время блокировки,сек
    private $time_token; // контролируемый период, сек
    private $count_request; // количестве запросов
    private $db; // количестве запросов


    public function __construct($time_block,$time_token, $count_request,$db)
    {
        $this->time_block = $time_block;
        $this->time_token = $time_token;
        $this->count_request  = $count_request;
        $this->db  = $db;
    }

    public function is_block($ip)
    {
        $now=time();
        $query = "SELECT * from ddos WHERE `ip`='$ip'";
        $result=$this->db->query($query);
        if ($result->num_rows == 1) { // имеется запись
            $data=$result->fetch_assoc();
            if ($data['block_time']>0) {

                if ($data['block_time']<$now) { // можно снимать с блокировки
                    $this->new_token($ip);
                    return false;
                }
                else
                    return ($data['block_time']-$now);
            }
            if ($data['token_time']<$now) { // пришло время выдать новый токен

                $this->new_token($ip);
                return false;
            }
            if ($data['request_limit']<=1) { // превышен лимит - блокируем

                $this->set_block($ip);
                return true;
            }
            else // уменьшаем количество разрешенных запросов
                $this->dec_limit($ip,$data['request_limit']);
        }
        else // запросов не было, создаем новыю запись
            $this->new_ip($ip);
    }

    public function dec_limit($ip,$current){
        $query="UPDATE ddos SET `request_limit`=".($current-1)." WHERE ip='$ip'";
        $result=$this->db->query($query);
        if(!$result) {
            printf("Ошибка выполения запроса 01");
            exit;
        }
    }

    public function new_ip($ip){
        $query="INSERT INTO ddos (`ip`, `token_time`, `request_limit`, `block_time`) VALUES ('$ip','".(time()+$this->time_token)."','".$this->count_request."','0')";
        $result=$this->db->query($query);
        if(!$result) {
            printf("Ошибка выполения запроса 02");
            exit;
        }
    }

    public function set_block($ip){
        $query="UPDATE ddos SET block_time=".(time()+$this->time_block)." WHERE ip='$ip'";
        $result=$this->db->query($query);
        if(!$result) {
            printf("Ошибка выполения запроса 03");
            exit;
        }
    }

    public function new_token($ip){
        $query="UPDATE ddos SET token_time=".(time()+$this->time_token).",request_limit=".$this->count_request.", block_time=0 WHERE ip='$ip'";
        $result=$this->db->query($query);
        if(!$result) {
            printf("Ошибка выполения запроса 04");
            exit;
        }
    }
}
?>